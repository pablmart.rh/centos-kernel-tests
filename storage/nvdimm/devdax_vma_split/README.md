# storage/nvdimm/devdax_vma_split

Storage: nvdimm devdax vma split testing, BZ1622171

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
